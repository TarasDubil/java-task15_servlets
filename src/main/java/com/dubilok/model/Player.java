package com.dubilok.model;

public class Player {

    private static int idPlayer;
    private int id;
    private String name;
    private int age;
    private String possition;

    public Player(String name, int age, String possition) {
        this.id = ++idPlayer;
        this.name = name;
        this.age = age;
        this.possition = possition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPossition() {
        return possition;
    }

    public void setPossition(String possition) {
        this.possition = possition;
    }
}

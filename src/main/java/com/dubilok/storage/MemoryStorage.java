package com.dubilok.storage;

import com.dubilok.model.Player;

import java.util.ArrayList;
import java.util.List;

public class MemoryStorage {
    private static final MemoryStorage memoryStorage;

    static {
        memoryStorage = new MemoryStorage();
    }

    private List<Player> players;

    private MemoryStorage() {
        this.players = new ArrayList<>();
        players.add(new Player("Dubilok", 20, "forward"));
        players.add(new Player("Nazar", 22, "deffender"));
        players.add(new Player("Zeri", 25, "forward"));
    }

    public static MemoryStorage getInstance() {
        return memoryStorage;
    }

    public List<Player> getPlayers() {
        return players;
    }
}

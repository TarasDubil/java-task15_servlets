package com.dubilok.reposetories;

import com.dubilok.model.Player;

import java.util.List;

public interface PlayerRepository {
    List<Player> findAll();

    void save(Player team);

    boolean isExist(String name);

    boolean delete(int id);

    boolean update(int id, String name, int age, String possition);
}

package com.dubilok.reposetories;

import com.dubilok.model.Player;
import com.dubilok.storage.MemoryStorage;

import java.util.List;

public class PlayerRepositoryImpl implements PlayerRepository {

    private MemoryStorage memoryStorage = MemoryStorage.getInstance();

    @Override
    public List<Player> findAll() {
        return memoryStorage.getPlayers();
    }

    @Override
    public void save(Player team) {
        memoryStorage.getPlayers().add(team);
    }

    @Override
    public boolean isExist(String name) {
        for (Player team : memoryStorage.getPlayers()) {
            if (team.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        Player player = null;
        for (int i = 0; i < memoryStorage.getPlayers().size(); i++) {
            if (memoryStorage.getPlayers().get(i).getId() == id) {
                player = memoryStorage.getPlayers().get(i);
            }
        }
        if (player != null) {
            memoryStorage.getPlayers().remove(player);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(int id, String name, int age, String possition) {
        for (int i = 0; i < memoryStorage.getPlayers().size(); i++) {
            if (memoryStorage.getPlayers().get(i).getId() == id) {
                memoryStorage.getPlayers().get(i).setAge(age);
                memoryStorage.getPlayers().get(i).setName(name);
                memoryStorage.getPlayers().get(i).setPossition(possition);
                return true;
            }
        }
        return false;
    }
}

package com.dubilok.servlets.player;

import com.dubilok.model.Player;
import com.dubilok.reposetories.PlayerRepository;
import com.dubilok.reposetories.PlayerRepositoryImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {
    private String index = "/WEB_INF/view/update.jsp";

    private PlayerRepository playerRepository;

    @Override
    public void init() throws ServletException {
        this.playerRepository = new PlayerRepositoryImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Player> playerList = playerRepository.findAll();
        playerList.sort(Comparator.comparingInt(Player::getId));
        req.setAttribute("players", playerList);
        req.getRequestDispatcher(index).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("del");
        playerRepository.delete(Integer.parseInt(id));
        resp.sendRedirect("/java_task15_Servlets_war/update");
    }
}

package com.dubilok.servlets.player;

import com.dubilok.model.Player;
import com.dubilok.reposetories.PlayerRepository;
import com.dubilok.reposetories.PlayerRepositoryImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@WebServlet("/updatePlayer")
public class UpdatePlayerServlet extends HttpServlet {

    private String index = "/WEB_INF/view/updatePlayer.jsp";

    private PlayerRepository playerRepository;

    @Override
    public void init() throws ServletException {
        this.playerRepository = new PlayerRepositoryImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Player> playerList = playerRepository.findAll();
        playerList.sort(Comparator.comparingInt(Player::getId));
        req.setAttribute("players", playerList);
        req.getRequestDispatcher(index).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");
        int age = Integer.parseInt(req.getParameter("age"));
        String possition = req.getParameter("possition");
        playerRepository.update(id, name, age, possition);
        resp.sendRedirect("/java_task15_Servlets_war/update");
    }
}
